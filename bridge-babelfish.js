/*jshint node:true*/
'use strict';

var ari = require('ari-client');
var util = require('util');
var config = require('config');
var Fs = require('fs')
var uuid = require('node-uuid');

const Transcripter = require('./transcripter');
const Translate = require('@google-cloud/translate');
const AWS = require('aws-sdk')
const synthesizer = new AWS.Polly(config.get('polly.config'));

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
  res.sendfile('html/index.html');
});

app.get('/styles.css', function(req, res){
  res.sendfile('html/styles.css');
});

http.listen(3000, function(){
  console.log('listening on localhost:3000');
});


ari.connect('http://localhost:8088', 'asterisk', 'asterisk', clientLoaded);

// handler for client being loaded
function clientLoaded (err, client) {
  if (err) {
    console.error;
  }

  // handler for StasisStart event
  function stasisStart(event, channel) {
    // ensure the channel is not a dialed channel
    if (event.application === 'bridge-babelfish') {
      // ensure the channel is not a dialed channel
      var dialed = event.args[0] === 'dialed';

      if (!dialed) {
        channel.answer(function(err) {
          if (err) {
            console.error;
          }

          console.log('Channel %s has entered our application %s', channel.name, event.application);

          var playback = client.Playback();
          channel.play({media: 'sound:pls-wait-connect-call'},
            playback, function(err, playback) {
              if (err) {
                console.error;
              }
          });

          originate(event, channel);
        });
      }
    }
    else {
      console.log('Channel %s has entered our application %s', channel.name, event.application);
      processSnoop(channel.id, event.args[0], event.args[1])
    }
  }

  function originate(event, channel) {
    var dialed = client.Channel();

    channel.on('StasisEnd', function(event, channel) {
      hangupDialed(channel, dialed);
    });

    dialed.on('ChannelDestroyed', function(event, dialed) {
      hangupOriginal(channel, dialed);
    });

    dialed.on('StasisStart', function(event, dialed) {
      joinMixingBridge(channel, dialed);
      createSnoop(channel.id, dialed.id);
      createSnoop(dialed.id, channel.id);
    });

    dialed.originate(
      {endpoint: event.args[0], app: 'bridge-babelfish', appArgs: 'dialed'},
      function(err, dialed) {
        if (err) {
          console.error;
        }
    });
  }

  // handler for original channel hanging up so we can gracefully hangup the
  // other end
  function hangupDialed(channel, dialed) {
    console.log(
      'Channel %s left our application, hanging up dialed channel %s',
      channel.name, dialed.name);

    // hangup the other end
    dialed.hangup(function(err) {
      // ignore error since dialed channel could have hung up, causing the
      // original channel to exit Stasis
    });
  }

  // handler for the dialed channel hanging up so we can gracefully hangup the
  // other end
  function hangupOriginal(channel, dialed) {
    console.log('Dialed channel %s has been hung up, hanging up channel %s',
      dialed.name, channel.name);

    // hangup the other end
    channel.hangup(function(err) {
      // ignore error since original channel could have hung up, causing the
      // dialed channel to exit Stasis
    });
  }

  // handler for dialed channel entering Stasis
  function joinMixingBridge(channel, dialed) {
    var bridge = client.Bridge();

    dialed.on('StasisEnd', function(event, dialed) {
      dialedExit(dialed, bridge);
    });

    dialed.answer(function(err) {
      if (err) {
        console.error;
      }
    });

    bridge.create({type: 'mixing'}, function(err, bridge) {
      if (err) {
        console.error;
      }

      console.log('Created bridge %s', bridge.id);

      addChannelsToBridge(channel, dialed, bridge);
    });
  }

  // handler for the dialed channel leaving Stasis
  function dialedExit(dialed, bridge) {
    console.log(
        'Dialed channel %s has left our application, destroying bridge %s',
        dialed.name, bridge.id);

    bridge.destroy(function(err) {
      if (err) {
        console.error;
      }
    });
  }

  // handler for new mixing bridge ready for channels to be added to it
  function addChannelsToBridge(channel, dialed, bridge) {
    let data = {
      'type': 'new_bridge',
      'bridgeid': bridge.id,
      'uniqueid1': channel.id,
      'uniqueid2': dialed.id
    };
    io.sockets.emit('broadcast', data);
    console.log(util.format("New Bridge: %s", JSON.stringify(data, null, 2)));

    bridge.addChannel({channel: [channel.id, dialed.id]}, function(err) {
      if (err) {
        console.error;
      }
    });
  }

  function createSnoop(channelID, bridgedID) {
    return new Promise(function (fulfill, reject){
      client.channels.snoopChannel({
        app: 'snoop-recording',
        appArgs: [channelID, bridgedID],
        spy: 'in',
        channelId: channelID
      },
      function (err, snoopChannel) {
        if(err){
          reject(err);
        } 
        else {
          fulfill(snoopChannel);
        }
      });
    });
  }

  function getChannelVar(channelID, chanVar) {
    return new Promise(function (fulfill, reject){
      client.channels.getChannelVar({
         channelId: channelID,
         variable: chanVar
        },
        function (err, variable) {
          if(err){
            reject(err);
          }
          else {
            fulfill(variable.value);
          }
        }
      );
    });
  }

  function channelPlay(channelID, soundName) {
    return new Promise(function (fulfill, reject){
      var playback = client.Playback();
      client.channels.play({
         media: 'sound:' + soundName,
         channelId: channelID,
         playbackId: playback.id
        },
        function (err, playback) {
          if(err){
            reject(err);
          }
          else {
            fulfill(playback);
          }
        }
      );
    });
  }

  function recordChannel(channelID, fileName) {
    return new Promise(function (fulfill, reject){
      client.channels.record({
        channelId: channelID,
        format: config.get('recordings.inFormat'),
        name: fileName
      },
      function (err, liverecording) {
        if(err){
          reject(err);
        } 
        else {
          var soundPath = config.get('recordings.path') + '/' + fileName + '.' + config.get('recordings.inFormat');
          fulfill(soundPath);
        }
      });
    });
  }

  function processSnoop(snoopID, channelID, bridgedID) {
    Promise.all([
      getChannelVar(channelID, 'CHANNEL(language)'),
      recordChannel(snoopID, channelID),
      getChannelVar(bridgedID, 'CHANNEL(language)'),
      getChannelVar(bridgedID, 'CALLERID(name)')
    ])
    .then(function (results) {
      var language = results[0];
      var soundPath = results[1];
      var target = results[2];
      var CallerIDName = results[3];

      var request = JSON.parse(JSON.stringify(config.get('google.speech')));
      request.config.languageCode = language;

      let data = {
        'type': 'new_channel',
        'uniqueid': channelID,
        'from': language,
        'to': target,
        'callerid_name': CallerIDName,
        'soundpath': soundPath
      };
      io.sockets.emit('broadcast', data);
      console.log(util.format("New Channel: %s", JSON.stringify(data, null, 2)));

      var translator = Translate(config.get('google.API'));
      var translateOpts = {
        from: language.split('-')[0],
        to: target.split('-')[0]
      };

      if (translateOpts.from != translateOpts.to) {
        var synthesizeParams = JSON.parse(JSON.stringify(config.get('polly.synthesize')));
        getVoiceId(target, config.get('polly.voice.gender'))
        .then(function (voiceID) {
          synthesizeParams.VoiceId = voiceID;
        })
        .catch(function (err) {
          console.log(util.format("VoiceError: %s. Using the default VoiceID", JSON.stringify(err)));
          synthesizeParams.VoiceId = config.get('polly.voice.default');
        });
      }
      var utteranceID = uuid.v4();
      var utterance = "";
      var transcripter = new Transcripter(soundPath, request)
      .on('result', function (transcript, confidence, isFinal) {
        let data = {
          'type': 'transcription',
          'uniqueid': bridgedID,
          'utterance_id': utteranceID,
          'confidence': confidence,
          'transcription': transcript,
          'is_final': isFinal
        };
        io.sockets.emit('broadcast', data);
        console.log(util.format("New Transcription: %s", JSON.stringify(data, null, 2)));

        if (isFinal) {
          if (translateOpts.from != translateOpts.to) {
            utterance = utteranceID;
            translator.translate(transcript, translateOpts)
            .then(function(translation) {
                let data = {
                  'type': 'translation',
                  'uniqueid': bridgedID,
                  'utterance_id': utterance,
                  'confidence': confidence,
                  'transcription': transcript,
                  'translation': translation[0],
                  'is_final': isFinal
                };
                io.sockets.emit('broadcast', data);
                console.log(util.format("New Translation: %s", JSON.stringify(data, null, 2)));
                synthesizeParams.Text = translation[0];
                synthesize(synthesizeParams, utterance)
                .then(function (soundName) {
                  channelPlay(bridgedID, soundName)
                  .then(function (playback) {
                    let data = {
                      'type': 'playback',
                      'uniqueid': bridgedID,
                      'utterance_id': utterance,
                      'sound_name': soundName
                    };
                    io.sockets.emit('broadcast', data);
                    console.log(util.format("New PlayBack: %s", JSON.stringify(data, null, 2)));
                  })
                  .catch(function (err) {console.log(util.format("PlaybackError: %s", JSON.stringify(err)))});
                });

            })
            .catch(function (err) {console.log(util.format("TranslationError: %s", JSON.stringify(err)))});
          }
          utteranceID = uuid.v4();
        }
      })
      .on('limit', function (data) {
        client.channels.get({
          channelId: channelID
        })
        .then(function (channel) {
          console.log(util.format("Reached transcription time limit. Relaunching Transcripter"));
          transcripter.startTranscription();
        })
        .catch(function (err) {console.log(util.format("Avoiding to relaunch Transcripter into a non existing channel"))});
      })
      .on('error', function (err) {console.log(util.format("TranscriptError: %s on channel: %s", err, channelID))});
      transcripter.startTranscription();
    }).catch(function (err) {console.log(util.format("PromiseError: %s", JSON.stringify(err)))});

  }

  client.on('StasisStart', stasisStart);

  client.start(['bridge-babelfish', 'snoop-recording']);
}

function getVoiceId(languageCode, gender) {
  var voiceID = "Joanna";
  var params = {
    LanguageCode: config.get('language')[languageCode]
  };
  return new Promise(function (fulfill, reject) {
    synthesizer.describeVoices(params, function(err, data) {
      if (err)
        reject(err);
      else {
        if(data.Voices.length > 0) {
          voiceID = data.Voices[0].Id;
        }
        for(var voice of data.Voices) {
          if(voice.Gender == gender) {
            voiceID = voice.Id;
            break;
          }
        }
      }
      fulfill(voiceID);
    });
  });
}

function synthesize(params, basename) {
  return new Promise(function (fulfill, reject) {
    synthesizer.synthesizeSpeech(params, function (err, data) {
      if(err) {
        reject(err);
      }
      else if(data) {
        if(data.AudioStream instanceof Buffer) {
          var soundName = config.get('recordings.path') + '/' + basename;
          Fs.writeFile(soundName + "." + config.get('recordings.outFormat'), data.AudioStream, function(err) {
            if(err) {
              reject(err);
            }
            fulfill(soundName);
          });
        }
      }
    });
  });
}
